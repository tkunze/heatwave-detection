# README



## Packages

Install list of packages with ...


Some packages have to be install with conda-forge:
`conda install -c conda-forge "esmpy<8.4" xarray-datatree xmip geoviews xarrayutils`

Pyleoclim have to be installed with pip after conda installations:
`pip install pyleoclim esmpy`
